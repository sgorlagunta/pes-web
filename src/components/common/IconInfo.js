import React, {PropTypes} from 'react';
// import {Link} from 'react-router';
import EnvironmentUtils from '../../utils/EnvironmentUtils';

const IconInfo = ({className, section, target, children, tooltip, isLink=true, isMyProfile=false}) => {

    const environment = EnvironmentUtils.get('environment');
    const rootPage = isMyProfile ? environment.myProfileHelp : environment.helpPage;
    const helpPage = section ? rootPage + section : rootPage; 

    return (
    isLink 
    ?
    <a 
        target={target || '_blank'} 
        className={className} 
        title={tooltip || 'Click here for further information'}
        href={helpPage}>
        <img src={require('../../../assets/images/icon_info.png')} className='pes-icon icon-info' />
        {children}
    </a>
    : 
    <img className={className ? className + ' pes-icon icon-info' : 'pes-icon icon-info'} src={require('../../../assets/images/icon_info.png')} />
    );
};

IconInfo.propTypes = {
    className: PropTypes.string,
    section: PropTypes.string
};


export default IconInfo;